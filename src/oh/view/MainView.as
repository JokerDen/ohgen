package oh.view {
import flash.display.Sprite;

import oh.view.sections.IconSectionView;
import oh.view.sections.ScreenshotSectionView;
import oh.view.sections.formatter.ImageSizeModel;

public class MainView extends Sprite {
    private var iconSection:IconSectionView;
    private var screenSection:ScreenshotSectionView;

    private var imageSizeModel:ImageSizeModel;

    public function MainView() {
        imageSizeModel = new ImageSizeModel();

        iconSection = new IconSectionView(350, imageSizeModel);
        addChild(iconSection);

        screenSection = new ScreenshotSectionView(350, imageSizeModel);
        addChild(screenSection);


        iconSection.x = 100;
        iconSection.y = 100;

        screenSection.x = 100;
        screenSection.y = 400;
    }
}
}
