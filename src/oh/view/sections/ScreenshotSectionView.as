package oh.view.sections {
import oh.view.sections.formatter.FormatterView;
import oh.view.sections.formatter.ImageSizeModel;

public class ScreenshotSectionView extends SectionView {
    private var screenshotFormatter:FormatterView;

    public function ScreenshotSectionView(width:Number, imageSizeModel:ImageSizeModel) {
        super(width);

        screenshotFormatter = new FormatterView(width);
        addChild(screenshotFormatter);

        screenshotFormatter.setUp(imageSizeModel.screenshotSizes, "/screenshot_");

        setHeader("Screenshot generator");
    }
}
}
