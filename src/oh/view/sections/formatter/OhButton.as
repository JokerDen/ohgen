package oh.view.sections.formatter {
import flash.display.Sprite;
import flash.events.MouseEvent;

public class OhButton extends Sprite {
    protected var view:Sprite;

    private var _isEnabled:Boolean = true;

    public var onClickHandler:Function = null;
    public var onClickArgs:Array = null;

    public function OhButton() {
        drawView();

        view.buttonMode = true;
        view.addEventListener(MouseEvent.CLICK, handleClick);
    }

    protected function drawView():void {

    }

    protected function handleClick(event:MouseEvent):void {
        if (onClickHandler != null) {
            onClickHandler.apply(this, onClickArgs);
        }
    }

    public function set isEnabled(isEnabled:Boolean):void {
        _isEnabled = isEnabled;

        view.mouseEnabled = _isEnabled;
        view.mouseChildren = _isEnabled;
    }

    public function get isEnabled():Boolean {
        return _isEnabled;
    }
}
}
