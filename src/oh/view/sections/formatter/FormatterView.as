package oh.view.sections.formatter {
import com.adobe.images.PNGEncoder;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.display.Sprite;
import flash.events.Event;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.geom.Matrix;
import flash.net.FileFilter;
import flash.net.FileReference;
import flash.text.TextField;
import flash.utils.ByteArray;

public class FormatterView extends Sprite {
    private var labelTF:TextField;
    private var chooseOriginalButton:ChooseFileButton;
    private var convertButton:ConvertButton;

    private var imageFile:FileReference;
    private var imageLoader:Loader;
    private var imageBitmap:Bitmap;

    private var outputFolder:File;
    private var _imageSizes:Vector.<ImageSizeData>;
    private var _fileName:String;

    public function FormatterView(width:Number) {
        labelTF = UITextFieldFactory.createHeader(width, 40, 20);
        addChild(labelTF);

        chooseOriginalButton = new ChooseFileButton();
        addChild(chooseOriginalButton);

        convertButton = new ConvertButton();
        addChild(convertButton);


        chooseOriginalButton.y = 25;

        convertButton.y = chooseOriginalButton.y;
        convertButton.x = width / 2;


        chooseOriginalButton.onClickHandler = openImageFileSelection;
        convertButton.onClickHandler = selectOutputFolderAndConvertImage;

        reset();
    }

    public function setUp(imageSizes:Vector.<ImageSizeData>, fileName:String):void {
        _fileName = fileName;
        _imageSizes = imageSizes;
    }

    private function reset():void {
        if (imageFile) {
            imageFile.removeEventListener(Event.SELECT, handleImageFileSelection);
            imageFile.removeEventListener(Event.COMPLETE, loadImageFromFile);
            imageFile = null;
        }
        if (imageLoader) {
            imageLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, handleImageLoadingCompleteEvent);
        }

        convertButton.isEnabled = false;
    }


    public function setLabel(label:String):void {
        labelTF.text = label;
    }

    // ********************
    // Select imageBitmap behavior

    private function openImageFileSelection():void {
        reset();

        imageFile = new FileReference();

//        var imageFileTypes:FileFilter = new FileFilter("PNG imageBitmap (need to be 1024x1024)", "*.png");
        var imageFileTypes:FileFilter = new FileFilter("PNG imageBitmap", "*.png");

        imageFile.addEventListener(Event.SELECT, handleImageFileSelection);
        imageFile.browse([imageFileTypes]);
    }

    private function handleImageFileSelection(event:Event):void {
        imageFile.addEventListener(Event.COMPLETE, loadImageFromFile);
        imageFile.load();
    }

    private function loadImageFromFile(event:Event):void {
        imageLoader = new Loader();

        imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleImageLoadingCompleteEvent);
        imageLoader.loadBytes(imageFile.data);
    }

    private function handleImageLoadingCompleteEvent(event:Event):void {
        imageBitmap = Bitmap(LoaderInfo(event.target).content);

        convertButton.isEnabled = true;
    }

    // *********************
    // Convert imageBitmap behavior

    private function selectOutputFolderAndConvertImage():void {
        outputFolder = File.documentsDirectory;
        outputFolder.browseForDirectory("Select output folder");
        outputFolder.addEventListener(Event.SELECT, convertAndSaveImages);
    }

    private function convertAndSaveImages(event:Event):void {
        if (imageBitmap) {
            for (var i:int = 0; i < _imageSizes.length; i++) {
                var imageSizeData:ImageSizeData = _imageSizes[i];

                var newImage:BitmapData = new BitmapData(imageSizeData.width, imageSizeData.height, false, 0xFFFFFF);
                var transform:Matrix = new Matrix();

                var scale:Number = Math.max(imageSizeData.width / imageBitmap.width, imageSizeData.height / imageBitmap.height);

                transform.scale(scale, scale);

                var offsetX:Number = -(imageBitmap.width * scale - imageSizeData.width) / 2;
                var offsetY:Number = -(imageBitmap.height * scale - imageSizeData.height) / 2;
                transform.translate(offsetX, offsetY);

                newImage.draw(imageBitmap.bitmapData, transform, null, null, null, true);
                var newImageBytes:ByteArray = PNGEncoder.encode(newImage);

                var fileName:String = outputFolder.nativePath + _fileName;
                fileName += imageSizeData.width + "x" + imageSizeData.height + ".png";

                var newImageFile:File = new File(fileName);

                var newImageStream:FileStream = new FileStream();
                newImageStream.open(newImageFile, FileMode.WRITE);
                newImageStream.writeBytes(newImageBytes);
                newImageStream.close();

                trace("Saved: " + fileName);
            }
            trace("Finished");
        }
    }
}
}
