package oh.view.sections.formatter {
public class ImageSizeData {
    private var _width:Number;
    private var _height:Number;

    public function ImageSizeData(width:Number, height:Number) {
        _width = width;
        _height = height;
    }

    public function get width():Number {
        return _width;
    }

    public function get height():Number {
        return _height;
    }
}
}
