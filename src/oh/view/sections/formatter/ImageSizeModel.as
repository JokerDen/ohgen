package oh.view.sections.formatter {
public class ImageSizeModel {
    private var _iosIconSizes:Vector.<ImageSizeData> = new <ImageSizeData>[];
    private var _androidIconSizes:Vector.<ImageSizeData> = new <ImageSizeData>[];

    private var _screenshotSizes:Vector.<ImageSizeData> = new <ImageSizeData>[];

    public function ImageSizeModel() {
        createMobileIcons();
        createScreenshots();
    }

    private function createMobileIcons():void {
        const ANDROID:String = "android";
        const IOS:String = "ios";
        const BOTH:String = "both";

        function createIconSize(size:Number, type:String):void {
            if (type == IOS || type == BOTH) {
                _iosIconSizes.push(new ImageSizeData(size, size));
            }
            if (type == ANDROID || type == BOTH) {
                _androidIconSizes.push(new ImageSizeData(size, size));
            }
        }

        // https://help.adobe.com/en_US/air/build/WS901d38e593cd1bac1e63e3d129907d2886-8000.html
        createIconSize(29, IOS);
        createIconSize(36, ANDROID);
        createIconSize(40, IOS);
        createIconSize(48, BOTH);
        createIconSize(50, IOS);
        createIconSize(57, IOS);
        createIconSize(58, IOS);
        createIconSize(60, IOS);
        createIconSize(72, BOTH);
        createIconSize(75, IOS);
        createIconSize(76, IOS);
        createIconSize(80, IOS);
        createIconSize(87, IOS);
        createIconSize(96, ANDROID);
        createIconSize(100, IOS);
        createIconSize(114, IOS);
        createIconSize(120, IOS);
        createIconSize(144, BOTH);
        createIconSize(152, IOS);
        createIconSize(167, IOS);
        createIconSize(180, IOS);
        createIconSize(192, ANDROID);
        createIconSize(512, BOTH);
        createIconSize(1024, IOS);
    }

    private function createScreenshots():void {

        function createScreenshot(width:Number, height:Number):void {
            _screenshotSizes.push(new ImageSizeData(width, height))
        }

        createScreenshot(1125, 2436);   // 5.8-inch (iPhone X) 1125x2436 (optional)
        createScreenshot(1242, 2208);   // 5.5-inch (retina) 1242x2208 (required for iPhone)
        createScreenshot(2048, 2732);   // 12.9-inch (retina) 2048x2732 (required for iPad)
        createScreenshot(1536, 2048);   // 7-inch tablet Android
        createScreenshot(1024, 500);    // `Feature Graphic` 1024x500 (no alpha)
        createScreenshot(180, 120);     // `Promo Graphic` 180x120 (no alpha)

        // iOS
        // some more sizes exists but compatible with listed
        // Maybe in future: macOS, tvOS, watchOS

        // Android
        // Screenshots: Phone, Tablet 7-inch, table 10-inch + Android TV, Android Wear
        // 1536x2048 as 7-inch?
        // `TV Banner` 1280x720 (no alpha)
        // `Daydream 360 degree stereoscopic image` 4096x4096
    }

    public function get iosIconSizes():Vector.<ImageSizeData> {
        return _iosIconSizes;
    }

    public function get androidIconSizes():Vector.<ImageSizeData> {
        return _androidIconSizes;
    }

    public function get screenshotSizes():Vector.<ImageSizeData> {
        return _screenshotSizes;
    }
}
}
