package oh.view.sections.formatter {
public class ChooseFileButton extends OhButton {

    public function ChooseFileButton() {
        super();
    }

    override protected function drawView():void {
        view = DrawViewFactory.createButtonView(75, 50, 0x6699FF);
        addChild(view);
    }
}
}
