package oh.view.sections.formatter {
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

public class UITextFieldFactory {
    private static const DEFAULT_FONT:String = null;

    public function UITextFieldFactory() {
    }

    public static function createHeader(width:Number, height:Number, size:Number):TextField {
        var tf:TextField = new TextField();
        tf.width = width;
        tf.height = height;
        tf.selectable = false;
        tf.defaultTextFormat = createTextFormat(size, true);
        return tf;
    }

    private static function createTextFormat(size:Number, bold:Boolean, align:String = TextFormatAlign.LEFT):TextFormat {
        return new TextFormat(DEFAULT_FONT, size, 0x000000, bold, null, null, null, null, align);
    }

    public static function createCaption(width:Number, height:Number, size:Number):TextField {
        var tf:TextField = new TextField();
        tf.width = width;
        tf.height = height;
        tf.selectable = false;
        tf.defaultTextFormat = createTextFormat(size, false);
        return tf;
    }
}
}
