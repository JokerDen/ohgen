package oh.view.sections.formatter {
import flash.display.Sprite;

public class DrawViewFactory {

    public static function createButtonView(width:Number, height:Number, color:uint):Sprite {
        var view:Sprite = new Sprite();
        view.graphics.beginFill(color);
        view.graphics.drawRect(0, 0, width, height);
        view.graphics.endFill();
        return view;
    }

    public function DrawViewFactory() {
    }
}
}
