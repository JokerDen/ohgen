package oh.view.sections {
import flash.display.Sprite;
import flash.text.TextField;

import oh.view.sections.formatter.UITextFieldFactory;

public class SectionView extends Sprite {
    private var headerTF:TextField;
    protected var content:Sprite;

    public function SectionView(width:Number) {
        headerTF = UITextFieldFactory.createCaption(width, 50, 24);
        addChild(headerTF);

        content = new Sprite();
        addChild(content);


        content.y = 50;
    }

    public function setHeader(headerText:String):void {
        headerTF.text = headerText;
    }
}
}
