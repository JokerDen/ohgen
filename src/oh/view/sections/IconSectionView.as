package oh.view.sections {
import oh.view.sections.formatter.FormatterView;
import oh.view.sections.formatter.ImageSizeModel;

public class IconSectionView extends SectionView {
    private var iosFormatter:FormatterView;
    private var androidFormatter:FormatterView;

    public function IconSectionView(sectionWidth:Number, imageSizeModel:ImageSizeModel) {
        super(sectionWidth);

        androidFormatter = new FormatterView(sectionWidth);
        content.addChild(androidFormatter);

        iosFormatter = new FormatterView(sectionWidth);
        content.addChild(iosFormatter);


        iosFormatter.y = 100;


        androidFormatter.setLabel("Android");
        iosFormatter.setLabel("iOS");

        androidFormatter.setUp(imageSizeModel.androidIconSizes, "/android/icon_");
        iosFormatter.setUp(imageSizeModel.iosIconSizes, "/ios/icon_");

        setHeader("Icon generator");
    }
}
}
