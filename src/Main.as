package {

import flash.display.Sprite;

import oh.view.MainView;

[SWF(backgroundColor="#FFFFFF", width="720", height="540", frameRate="60")]
public class Main extends Sprite {
    private var view:MainView;

    public function Main() {

        view = new MainView();
        addChild(view);
    }
}
}
